# Containerization

## Quick-Start

    # Download Repo
    git clone http://192.168.15.138/techworks/gantry.git
    cd gantry/ansible/
    
    # Initialize Tools
    ./init-metal.sh
    
    # Initialize Gantry VM
    ./init-gantry.py
    
    # Login to Gantry VM
    ssh -i keys/engility.pem ubuntu@172.28.128.10
    cd /mnt/ansible/
        
    # Setup LXD (accept all defaults)
    lxd init
        
    # Launch Container    
    lxc launch ubuntu:16.04 ldap

    # Initialize Container (mount volumes, add keys)
    ./init-container.sh ldap

    # List Hosts 
    lxc list
    
    # Add host IP address to hosts file
    nano hosts

    # Run Ansible Playbook against host
    ansible-playbook -vvvv playbooks/deploy-ldap.yml -i hosts -l ldap

    
    
## Definition

__Containerization is a light-weight virtualization technology, which facilitates the modularity of software, by providing a micro-service friendly operating environment.__

    - Containers : Linux Containers (LXC) provides the foundation of Containerization, and is basically the "bare-metal" version of Docker.
    - Infrastructure as Code : Ansible provides an IaC framework, enabling us to install local &/or multiple remote servers via code.


## Approach

    Initial containerization of our development environment splits our infrastructure into 3 separate containers :
        - "team-workspace"
        - "team-db"
        - "team-web"

    Future iterations will target containerization of independently deployable web-apps :
        - "team-workspace"
        - "team-db"
        - "team-ui"
        - "team-cms"
        - "team-nwq"
        - "team-security"

    Iteratively converting components reduces the risk of migrating team modules, from it's current state, into message-oriented micro-services.
    
    

## High-Level Architecture  

We provide a simple Python wrapper, which drives both LXC & Ansible APIs 

![rtc_workspace_id](http://10.0.0.117/img/team-containerization.jpg "Getting rtc-workspace-id")

__network-bridge__ : LXC attaches to either an internal "laptop-only" network, or may be bridged to the external "floating/public" network.

__lxc-containers__ : Containers are accessible via ssh, "lxc-attach" command, or through root folder-based access on the hypervisor system.

__team-driver__ : Python script manipulates containers using LXC API, or launches ansible-playbooks using Ansible API.
    
__ansible-playbooks__: Idempotent, state-based, playbooks configure openconnect, common, java, maven, workspace, db, and server installation.



---
---

# Using LXC

### SSH into Container

    ssh -i keys/engility.pem ubuntu@192.168.xxx.xxx

### Bash : LXC command line tools

    - lxc list
    - lxc launch ubuntu:17.10 dashboard
    - lxc config device add dashboard sharedtmp disk path=../ source=/mnt
    - lxc exec dashboard -- bash
    - lxc delete -f dashboard


---
---

# Using Ansible

Ansible has 5 main concepts : 

## Hosts

    - A file called "hosts" is used to map server-groups to container IP addresses
    - Playbooks may be run against "1 to many" hosts at a time.

## Playbooks 

    - Each Playbook represents a independently deployable "procedure". 
    - Playbooks may contain multiple roles which function as the component building blocks of a larger process.
    - Examples "install-server", install-db", or "updated-db"
         
## Roles
    - Each Role is like a single lego block.
    - Scoped by the Unix philosophy "make each program do one thing well".
    - Roles should be "stackable", & play well with other roles in a logical ordering.
    - Examples : "install-maven", install-java, or "install-workspace"             
    
## Tasks
    - Each Role is composed of multiple "CLI-equivalent" Tasks.
    - Each task is state-based (enforces state, not blind-operation)
    - Each task is idempotent (won't execute twice)
    - Examples : create, copy, or delete files, append-file, apt-get update/upgrade/install, shell (CLI) 


## Vars

    - Configuration-File : "vars/user_vars.yml"



### Ansible CLI

Example : Running Ansible playbooks from command-line w/o our python-driver

    0) Install teambox
    ansible-playbook install-team.yml -i hosts -vvvv
    
    1) Build teambox
    ansible-playbook build-team.yml -i hosts -vvvv
    
    2) Update teambox
    ansible-playbook update-team.yml -i hosts -vvvv


---
## Logging / Monitoring

    Use "tail" on VM server to monitor installation progress.
    tail -f /opt/outputfile.txt


---
## CLI-based VPN access

    (echo -n "password" | openconnect mycloud.devolution.com --no-cert-check --user=username --authgroup=TFG-027-7 --passwd-on-stdin &) &
