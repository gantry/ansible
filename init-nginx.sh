#!/usr/bin/env bash
CONTAINER_NAME=$1

#lxc launch ubuntu:14.04 $CONTAINER_NAME

echo "Setting up : $CONTAINER_NAME "


# Grep "lxc info <container>" & then Awk IP Address from output
# https://www.brianparsons.net/FindIPAddresseswithawk/
HOST_IP=$(lxc info $CONTAINER_NAME | grep eth0 | grep inet | grep -v inet6 | awk '{match($0,/[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/); ip = substr($0,RSTART,RLENGTH); print ip}')

# Add container name & ip address to nginx
sudo sed -i -e "s/$CONTAINER_NAME.local/$HOST_IP/g" /etc/nginx/sites-available/default
