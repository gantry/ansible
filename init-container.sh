#!/usr/bin/env bash
CONTAINER_NAME=$1


#lxc launch ubuntu:14.04 $CONTAINER_NAME

echo "Setting up : $CONTAINER_NAME "

# Give Container time to spin-up
sleep 5

# Install Ansible Dependencies
lxc exec $CONTAINER_NAME -- apt-get update
lxc exec $CONTAINER_NAME -- apt-get install ssh python htop iftop tree -y

# Add Certificates
#cat keys/engility.pub > /var/lib/lxd/containers/$CONTAINER_NAME/rootfs/home/ubuntu/.ssh/authorized_keys
lxc file push --uid=1000 --gid=1000 ../keys/engility.pub $CONTAINER_NAME/home/ubuntu/.ssh/authorized_keys

# Add Shared directory to Container
lxc config device add $CONTAINER_NAME sharedtmp disk path=/mnt source=$(pwd)
lxc list


echo "Updating Hosts"

# Remove existing Container from hosts file (if exists)
sed -i "/$CONTAINER_NAME/d" hosts

# Grep "lxc info <container>" & then Awk IP Address from output
# https://www.brianparsons.net/FindIPAddresseswithawk/
HOST_IP=$(lxc info $CONTAINER_NAME | grep eth0 | grep inet | grep -v inet6 | awk '{match($0,/[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/); ip = substr($0,RSTART,RLENGTH); print ip}')

# Construct hosts file entry
# test0 ansible_ssh_host=10.64.254.102 ansible_ssh_user=ubuntu ansible_ssh_private_key_file=keys/engility.pem
HOSTS_ENTRY="$CONTAINER_NAME ansible_ssh_host=$HOST_IP ansible_ssh_user=ubuntu ansible_ssh_private_key_file=../keys/engility.pem"

echo "New Hosts Entry"
echo $HOSTS_ENTRY
echo $HOSTS_ENTRY >> hosts

# Add container name & ip address to nginx
sudo sed -i -e 's/$CONTAINER_NAME.local/$HOST_IP/g' /etc/nginx/sites-available/default
